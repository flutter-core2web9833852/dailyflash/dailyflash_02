import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State createState() => _MainApp();
}

class _MainApp extends State {
  int count = 1;

  void nextQuestion() {
    setState(() {
      count++;
    });
  }

  Scaffold question() {
    if (count == 1) {
      return Scaffold(
        body: Center(
          child: Container(
            height: 200,
            width: 200,
            transformAlignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.red)),
            child: const Center(
              child: Text(
                "container 1",
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      );
    } else if (count == 2) {
      return Scaffold(
        body: Center(
          child: Container(
            height: 100,
            width: 100,
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
                border: Border(
              left: BorderSide(color: Colors.red, width: 5),
              top: BorderSide(color: Colors.black, width: 1),
              bottom: BorderSide(color: Colors.black, width: 1),
              right: BorderSide(color: Colors.black, width: 1),
            )),
            child: const Text(
              "container 2",
            ),
          ),
        ),
      );
    } else if (count == 3) {
      return Scaffold(
        body: Center(
          child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
                borderRadius:
                    const BorderRadius.only(topRight: Radius.circular(20)),
                border: Border.all(color: Colors.green)),
          ),
        ),
      );
    } else if (count == 4) {
      return Scaffold(
        body: Center(
          child: Container(
            height: 100,
            width: 300,
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
                border: Border.all(color: Colors.lightBlueAccent)),
            child: const Text(
              "Khushal Parmar",
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Center(
            child: GestureDetector(
          onTap: () {
            setState(() {
              text = "Container Tapped!";
              flag = true;
            });
          },
          child: Container(
            height: 100,
            width: 200,
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: (flag)?Colors.blue:Colors.red,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.lightBlueAccent)),
            child: Text(
              text
            ),
          ),
        )),
      );
    }
  }

  String text = "click me!";
  bool flag = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: question(),
      floatingActionButton: FloatingActionButton(
        onPressed: nextQuestion,
        child: const Text("Next"),
      ),
    ));
  }
}
